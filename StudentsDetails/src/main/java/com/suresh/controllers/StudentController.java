package com.suresh.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.suresh.dto.Students;
import com.suresh.service.StudentsService;

@RestController
public class StudentController {
	@Autowired
	StudentsService service;
	
	@PostMapping("/addStudent")
	public Object addStudent(@RequestBody Students student) {
		return service.addStudent(student);
	}
	
	@PutMapping("/modifyStudent")
	public Students modifyStudent(@RequestBody Students student) {
		return service.modifyStudent(student);
	}
	
	@GetMapping("/getStudentById")
	public Optional<Students> getStudentById(@RequestParam int id)
	{
		return service.getStudentById(id);
	}
	
	@GetMapping("/getStudentByName/{name}")
	public Optional<Students> getStudentByName(@PathVariable String name)
	{
		return service.getStudentByName(name);
	}
	
	@DeleteMapping("deleteStudent")
	public String deleteStudent(@RequestParam int id) {
		return service.deleteStudent(id);
	}
}
