package com.suresh.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.suresh.dto.Students;

@Repository
public interface StudentDao extends CrudRepository<Students,Integer>{

	Optional<Students> findByName(String name);

}
