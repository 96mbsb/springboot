package com.suresh.StudentsDetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.suresh.controllers", "com.suresh.service"})
@EnableJpaRepositories("com.suresh.dao")
@EntityScan("com.suresh.dto")
public class StudentsDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsDetailsApplication.class, args);
	}

}
