package com.suresh.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.suresh.dao.StudentDao;
import com.suresh.dto.Students;

@Service
public class StudentsService {
	
	@Autowired
	StudentDao dao;
	
	public Object addStudent(Students student) {
		Optional<Students> optional= dao.findById(student.getId());
		if(optional.isEmpty()) 
			return dao.save(student);
		else
			return "Already id is present";
				
	}

	public Optional<Students> getStudentById(int id) {
		return dao.findById(id);
	}

	public Optional<Students> getStudentByName(String name) {
		return dao.findByName(name);
	}

	public String deleteStudent(int id) {
		Optional<Students> optional= dao.findById(id);
		 if(!optional.isEmpty()) {
			 dao.deleteById(id);
			 return "Deleted";
		 }
		 return "Data not available";
	}

	public Students modifyStudent(Students student) {
		return dao.save(student);
	}
	
}
